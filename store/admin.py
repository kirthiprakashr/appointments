from django.contrib import admin

from store.models import Resource, Session, Slot, User

admin.site.register(User)
admin.site.register(Resource)
admin.site.register(Session)
admin.site.register(Slot)
