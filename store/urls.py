"""
author: kp
date: 1/11/16
"""
from django.conf.urls import url
from django.views.decorators.csrf import csrf_exempt

from store import views

urlpatterns = [
    url(r'^resource/$', views.list_resources, name='list_resources'),
    url(r'^session.html/$', views.SessionHtmlView.as_view()),
    url(r'^session/create/$', csrf_exempt(views.SessionCreateView.as_view())),
    url(r'^session/(?P<session_id>\d+)/slots.html/$', views.SessionSlotHtmlView.as_view()),
    url(r'^session/(?P<session_id>\d+)/slot/(?P<slot_id>\d+)/book/$', views.SessionSlotBookView.as_view()),
    url(r'^session/(?P<session_id>\d+)/slot/$', views.SessionSlotView.as_view()),
    url(r'^session/$', views.SessionView.as_view()),
]
