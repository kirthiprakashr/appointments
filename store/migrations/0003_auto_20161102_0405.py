# -*- coding: utf-8 -*-
# Generated by Django 1.9.8 on 2016-11-02 04:05
from __future__ import unicode_literals

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('store', '0002_sessionslot_price'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='SessionSlot',
            new_name='SlotDef',
        ),
    ]
