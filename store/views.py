import json

from datetime import datetime, timedelta
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader

from store.models import Resource, Session, Slot
from django.views.generic import View


def list_resources(request):
    resources_arr = []
    resource_qs = Resource.objects.all()
    for resource in resource_qs:
        resources_arr.append(resource.model_to_dict())
    resources_json = json.dumps(resources_arr)
    return HttpResponse(resources_json)


class SessionCreateView(View):
    def get(self, request):
        resources = Resource.objects.filter()
        template = loader.get_template('session_form.html')
        return HttpResponse(
                template.render({'resources': resources, 'slot_duration_unit_choices': dict(Session.DURATION_UNITS)},
                                request))

    def post(self, request):
        resource_id = request.POST.get('resource_id')
        start_time_str = request.POST.get('start_time')
        start_time = datetime.strptime(start_time_str, Session.DATETIME_FORMAT)
        end_time_str = request.POST.get("end_time")
        end_time = datetime.strptime(end_time_str, Session.DATETIME_FORMAT)
        slot_duration_str = request.POST.get("slot_duration")
        slot_duration = int(slot_duration_str)
        slot_duration_unit = request.POST.get("slot_duration_unit")
        slot_price_str = request.POST.get("slot_price")
        slot_price = float(slot_price_str)

        params = {"resource_id": resource_id, "start_time": start_time, "end_time": end_time,
                  "slot_duration": slot_duration, "slot_duration_unit": slot_duration_unit, "slot_price": slot_price}
        session = Session.objects.create(**params)
        duration = timedelta(**{
            Session.SLOT_DURATION_TIMEDELTA_MAPPING[slot_duration_unit]: slot_duration
        })
        while start_time < end_time:
            params = {
                'session_id': session.id,
                'start_time': start_time,
                'status': Slot.STATUS_FREE
            }
            Slot.objects.create(**params)
            start_time += duration
        return HttpResponseRedirect("/store/session.html/")


class SessionView(View):
    def get(self, request):
        resource_id = request.GET.get('resource_id')
        session_qs = Session.objects.filter()
        if resource_id:
            session_qs = session_qs.filter(resource_id=resource_id)
        session_arr = []
        for session in session_qs:
            session_arr.append(session.model_to_dict(unix_timestamp=True))
        sessions_json = json.dumps(session_arr)
        return HttpResponse(sessions_json)

class SessionHtmlView(View):
    def get(self, request):
        template = loader.get_template('session.html')
        return HttpResponse(template.render({}, request))


class SessionSlotHtmlView(View):
    def get(self, request, session_id):
        template = loader.get_template('slots.html')
        return HttpResponse(template.render({"session_id": session_id}, request))


class SessionSlotView(View):
    def get(self, request, session_id):
        slot_arr = []
        slot_qs = Slot.objects.filter(session_id=session_id).order_by('start_time')
        for slot in slot_qs:
            slot_arr.append(slot.model_to_dict(unix_timestamp=True))
        slots_json = json.dumps(slot_arr)
        return HttpResponse(slots_json)


class SessionSlotBookView(View):
    def get(self, request, session_id, slot_id):
        try:
            slot = Slot.objects.get(session_id=session_id, id=slot_id)
        except Exception as e:
            return HttpResponse("Invalid slot. {}; {}; {}".format(slot_id, session_id, e))

        if slot.status != Slot.STATUS_FREE:
            return HttpResponse("Slot is not FREE. Try another slot")

        slot.status = Slot.STATUS_BLOCKED
        slot.save()

        return HttpResponseRedirect("/store/session/{}/slots.html".format(session_id))
