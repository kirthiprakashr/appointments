from __future__ import unicode_literals

import time
from django.db import models


class User(models.Model):
    name = models.CharField(max_length=256)


class Resource(models.Model):
    """
    Resource could be Doctor, Lawyer etc..
    """
    name = models.CharField(max_length=256)
    description = models.TextField()

    def __unicode__(self):
        return self.name

    def model_to_dict(self):
        return {"id": self.id, "name": self.name, "description": self.description}


class SlotDef(models.Model):
    DURATION_UNIT_MINUTE = 'MINUTE'
    DURATION_UNIT_HOUR = 'HOUR'
    DURATION_UNIT_DAY = 'DAY'
    DURATION_UNITS = ((DURATION_UNIT_MINUTE, DURATION_UNIT_MINUTE), (DURATION_UNIT_HOUR, DURATION_UNIT_HOUR),
                      (DURATION_UNIT_DAY, DURATION_UNIT_DAY))
    duration = models.FloatField()
    duration_unit = models.CharField(max_length=64, choices=DURATION_UNITS, default=DURATION_UNIT_MINUTE)
    price = models.FloatField()

    def __unicode__(self):
        return "{} | {} | {}".format(self.duration, self.duration_unit, self.price)

    def model_to_dict(self):
        return {"duration": self.duration, "duration_unit": self.duration_unit, "price": self.price}


class Session(models.Model):
    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    DURATION_UNIT_MINUTE = 'MINUTE'
    DURATION_UNIT_HOUR = 'HOUR'
    DURATION_UNIT_DAY = 'DAY'
    DURATION_UNITS = ((DURATION_UNIT_MINUTE, DURATION_UNIT_MINUTE), (DURATION_UNIT_HOUR, DURATION_UNIT_HOUR),
                      (DURATION_UNIT_DAY, DURATION_UNIT_DAY))

    SLOT_DURATION_TIMEDELTA_MAPPING = {
        'MINUTE': 'minutes',
        'HOUR': 'hours',
        'DAY': 'days'
    }

    resource = models.ForeignKey(Resource)
    start_time = models.DateTimeField()
    end_time = models.DateTimeField()
    slot_duration = models.FloatField()
    slot_duration_unit = models.CharField(max_length=64, choices=DURATION_UNITS, default=DURATION_UNIT_MINUTE)
    slot_price = models.FloatField()

    # booking_open_time = models.DateTimeField()
    # booking_close_time = models.DateTimeField()

    def __unicode__(self):
        return "{} | {} | {} | {} | {}".format(self.start_time, self.resource.name, self.slot_duration,
                                               self.slot_duration_unit,
                                               self.slot_price)

    def model_to_dict(self, unix_timestamp=False):
        return {"id": self.id,
                "resource_id": self.resource.id,
                "resource_name": self.resource.name,
                "start_time": time.mktime(self.start_time.timetuple()) if unix_timestamp else self.start_time.strftime(
                        '%Y-%m-%d %H:%M:%S'),
                "end_time": time.mktime(self.end_time.timetuple()) if unix_timestamp else self.end_time.strftime(
                        '%Y-%m-%d %H:%M:%S'),
                "slot_duration": self.slot_duration,
                "slot_duration_unit": self.slot_duration_unit,
                "slot_price": self.slot_price}


class Slot(models.Model):
    DATETIME_FORMAT = "%Y-%m-%d %H:%M:%S"

    STATUS_FREE = 'FREE'
    STATUS_BOOKED = 'BOOKED'
    STATUS_BLOCKED = 'BLOCKED'  # payment under process or pending
    STATUS_CHOICES = (
        (STATUS_FREE, STATUS_FREE), (STATUS_BLOCKED, STATUS_BLOCKED), (STATUS_BOOKED, STATUS_BOOKED))

    session = models.ForeignKey(Session)
    start_time = models.DateTimeField()
    status = models.CharField(max_length=32, choices=STATUS_CHOICES)

    def __unicode__(self):
        return "{} | {} | {}".format(self.session.id, self.start_time, self.status)

    def model_to_dict(self, unix_timestamp=False):
        return {"id": self.id,
                "session_id": self.session.id,
                "start_time": time.mktime(self.start_time.timetuple()) if unix_timestamp else self.start_time.strftime(
                        '%Y-%m-%d %H:%M:%S'),
                "status": self.status}
