"""
author: kp
date: 2/11/16
"""
from django.http import HttpResponseRedirect


def index(request):
    return HttpResponseRedirect("/store/session.html/")
